
import Nav from './Nav';
import AttendeesList from './attendees/AttendeesList';
import LocationForm from './locations/LocationForm';
import LocationList from './locations/LocationList';
import ConferenceForm from './conference/ConferenceForm';
import AttendConferenceForm from './attendees/AttendConferenceForm';
import PresentationForm from "./presentations/PresentationForm";
import ConferenceList from "./conference/conferenceList"


function App(props) {
  return (
    <>
      <Nav />
      {/* <AttendConferenceForm /> */}
      <ConferenceList />
      {/* <ConferenceForm /> */}
        {/* <LocationForm /> */}
        {/* <AttendeesList attendees={props.attendees} /> */}
    </>
  );
}

export default App;
