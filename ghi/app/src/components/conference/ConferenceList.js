import App from '../App';
import { useState, useEffect } from 'react';

function ConferenceList(props) {
    const [conferences, setConferences] = useState=([])

    const getData = async () => {
        const response = await fetch('http://localhost:8000/api/conferences/');
        const data = await response.json();
        console.log(data);
        setConferences(data.conferences);
    }

    useEffect(()=> {
        //execute code after component first renders
        getData()
    }, [])

    return (
        <>
        <App />
            <div className="container">
                <table className="table table-striped">
                    <thead>
                        <tr>
                        <th>id</th>
                        <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {conferences.map(conference => {
                            return (
                                <tr key={conference.href}>
                                <td>{ conference.id }</td>
                                <td>{ conference.name }</td>
                                </tr>
                            );
                    })}
                    </tbody>
                </table>
            </div>
        </>
    );
}

export default ConferenceList;
